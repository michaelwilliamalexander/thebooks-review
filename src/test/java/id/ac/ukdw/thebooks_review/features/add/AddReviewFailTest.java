package id.ac.ukdw.thebooks_review.features.add;

import id.ac.ukdw.thebooks_review.config.ObjectMapping;
import id.ac.ukdw.thebooks_review.controller.ReviewController;
import id.ac.ukdw.thebooks_review.dto.request.ReviewRequest;
import id.ac.ukdw.thebooks_review.exception.BadRequestException;
import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AddReviewFailTest {

    @InjectMocks
    private ReviewController controller;

    @Mock
    private ReviewService service;

    private MockMvc mockMvc;

    private List<Review> dataReview;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataReview = new ArrayList<>();
    }

    @Given("^Terdapat data-data Review$")
    public void data(List<Review> data) {
        dataReview = data;
    }

    @When("^user beremail (.*) ingin menambahkan data review (.*) , (.*) , (.*) , (.*)$")
    public void input(final String email,
                      final String isbn,
                      final double rating,
                      final String review,
                      final String timeReview) {

        when(service.saveReview(isbn,email,rating,review, timeReview)).thenThrow(new BadRequestException());
    }
    @Then("^user mendapatkan Bad Request dari data input (.*) , (.*) , (.*) , (.*) , (.*)$")
    public void output(final String email,
                       final String isbn,
                       final double rating,
                       final String review,
                       final String timeReview) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/review/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapping.asJsonString(
                        new ReviewRequest(
                                isbn,
                                email,
                                rating,
                                review,
                                timeReview
                        ))))
                .andExpect(status().isBadRequest())
                .andDo(print());

        verify(service).saveReview(isbn,email,rating,review,timeReview);
    }
}
