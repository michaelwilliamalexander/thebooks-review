package id.ac.ukdw.thebooks_review.features.get;

import id.ac.ukdw.thebooks_review.config.ObjectMapping;
import id.ac.ukdw.thebooks_review.controller.ReviewController;
import id.ac.ukdw.thebooks_review.dto.ReviewDTO;
import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllReviewTest {

    @InjectMocks
    private ReviewController controller;

    @Mock
    private ReviewService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Review> dataReview;

    private List<ReviewDTO> resultData;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataReview = new ArrayList<>();
        resultData = new ArrayList<>();
    }

    @Given("List data review")
    public void data(List<Review> data) {
        dataReview = data;
    }

    @When("^user mendapatkan review dengan isbn (.*)$")
    public void input(final String isbn) {
        for (Review review:dataReview){
            if (review.getIsbn().equals(isbn)){
                resultData.add(mapper.map(review,ReviewDTO.class));
            }
        }
        when(service.getAllReview(isbn)).thenReturn(resultData);
    }
    @Then("^user mendapatkan semua data review dengan data request (.*)$")
    public void output(final String isbn) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/review/buku/{isbn}",isbn))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data",hasSize(resultData.size())));

        verify(service).getAllReview(isbn);
    }
}
