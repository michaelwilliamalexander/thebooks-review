package id.ac.ukdw.thebooks_review.features.get;

import id.ac.ukdw.thebooks_review.controller.ReviewController;
import id.ac.ukdw.thebooks_review.exception.NotFoundException;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllReviewFailTest {

    @InjectMocks
    private ReviewController controller;

    @Mock
    private ReviewService service;

    private MockMvc mockMvc;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @When("^user mendapatkan data review dengan isbn (.*)$")
    public void input(final String isbn) {
        when(service.getAllReview(isbn)).thenThrow(new NotFoundException());
    }

    @Then("^user mendapatkan semua review dengan data request (.*)$")
    public void output(final String isbn) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/review/buku/{isbn}",isbn))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(service).getAllReview(isbn);
    }
}
