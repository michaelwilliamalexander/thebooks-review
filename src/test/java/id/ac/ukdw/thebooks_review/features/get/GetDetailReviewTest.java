package id.ac.ukdw.thebooks_review.features.get;

import id.ac.ukdw.thebooks_review.config.ObjectMapping;
import id.ac.ukdw.thebooks_review.controller.ReviewController;
import id.ac.ukdw.thebooks_review.dto.ReviewDTO;
import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetDetailReviewTest {

    @InjectMocks
    private ReviewController controller;

    @Mock
    private ReviewService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Review> dataReview;

    private ReviewDTO resultData;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataReview = new ArrayList<>();
    }

    @Given("^Terdapat data review$")
    public void data(List<Review> data) {
        dataReview = data;
    }

    @When("^user mendapatkan data review dengan data request (.*)$")
    public void input(final String idReview) {
        for (Review review: dataReview){
            if (review.getIdReview() == Integer.parseInt(idReview)){
                resultData = mapper.map(review, ReviewDTO.class);
                when(service.getDetailReview(Integer.parseInt(idReview))).thenReturn(resultData);
            }
        }
    }
    @Then("^user mendapatkan detail data review dari data request (.*)$")
    public void output(final String idReview) throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/review/{id}",idReview))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.data.idReview",is(resultData.getIdReview())))
                .andExpect(jsonPath("$.data.rating",is(resultData.getRating())))
                .andExpect(jsonPath("$.data.isbn",is(resultData.getIsbn())))
                .andExpect(jsonPath("$.data.review",is(resultData.getReview())))
                .andExpect(jsonPath("$.data.emailUser",is(resultData.getEmailUser())));

        verify(service).getDetailReview(Integer.parseInt(idReview));
    }
}
