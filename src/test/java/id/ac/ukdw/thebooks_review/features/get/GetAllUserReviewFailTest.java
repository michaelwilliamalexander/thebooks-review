package id.ac.ukdw.thebooks_review.features.get;

import id.ac.ukdw.thebooks_review.config.ObjectMapping;
import id.ac.ukdw.thebooks_review.controller.ReviewController;
import id.ac.ukdw.thebooks_review.exception.NotFoundException;
import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.cucumber.java.Before;
import io.cucumber.java.en.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class GetAllUserReviewFailTest {

    @InjectMocks
    private ReviewController controller;

    @Mock
    private ReviewService service;

    @Autowired
    private ModelMapper mapper;

    private MockMvc mockMvc;

    private List<Review> dataReview;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        dataReview = new ArrayList<>();
    }

    @Given("^Terlist data-data review$")
    public void data(List<Review> data) {
        dataReview = data;
    }

    @When("^user dengan email (.*) ingin mendapatkan semua data review mereka$")
    public void input(final String email) {
        when(service.getAllReviewByEmail(email)).thenThrow(new NotFoundException());
    }
    @Then("^user mendapatkan not found dari pengambilan semua data review mereka dengan request (.*)$")
    public void output(final String email) throws Exception{

        mockMvc.perform(MockMvcRequestBuilders.get("/review/user/")
                .param("email",email)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print());

        verify(service).getAllReviewByEmail(email);
    }


}
