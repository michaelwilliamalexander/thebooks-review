Feature: Mendapatkan data-data review

  Scenario Outline: Mendapatkan detail data review dan Ditemukan
    Given Terdapat data review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
      |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user mendapatkan data review dengan data request <idReview>
    Then user mendapatkan detail data review dari data request <idReview>
    Examples:
      |idReview |
      |1        |
      |3        |

  Scenario Outline: Mendapatkan detail data review dan Tidak ditemukan
    Given Terdapat data-data review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
      |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user mendapatkan data review dengan request <idReview>
    Then user mendapatkan detail data review dari request <idReview>
    Examples:
      |idReview|
      |5       |

  Scenario Outline: Mendapatkan semua data review dan ditemukan
    Given List data review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
      |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user mendapatkan review dengan isbn <isbn>
    Then user mendapatkan semua data review dengan data request <isbn>
    Examples:
    |isbn           |
    |9781338132311  |
    |9780804171588  |

  Scenario Outline: Mendapatkan semua data review dan tidak ditemukan
    When user mendapatkan data review dengan isbn <isbn>
    Then user mendapatkan semua review dengan data request <isbn>
    Examples:
      |isbn           |
      |9781338132311  |
      |9780804171588  |

  Scenario Outline: Mendapatkan data review dari User dan ditemukan
    Given Terlist data review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
      |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user dengan email <emailUser> ingin mendapatkan semua data reviewnya
    Then user mendapatkan  semua data review mereka dengan request <emailUser>
    Examples:
      |emailUser                      |
      |nathaniel.alvin@ti.ukdw.ac.id  |
      |michael.william@ti.ukdw.ac.id  |

  Scenario Outline: Mendapatkan data review dari User dan tidak ditemukan
    Given Terlist data-data review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user dengan email <emailUser> ingin mendapatkan semua data review mereka
    Then user mendapatkan not found dari pengambilan semua data review mereka dengan request <emailUser>
    Examples:
      |emailUser                      |
      |michael.william@ti.ukdw.ac.id  |


