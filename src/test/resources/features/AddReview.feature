Feature: Menambahkan Review

  Scenario Outline: Menambahkan data Review dan Berhasil
    Given Terdapat data Review
    |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
    |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
    |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
    |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
    |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user dengan email <email> ingin menambahkan data review <isbn> , <rating> , <review> , <timeReview>
    Then user mendapatkan pesan <pesan> dari data input <email> , <isbn> , <rating> , <review> , <timeReview>
    Examples:
    |email                              |isbn           |rating         |review           |timeReview          |pesan          |
    |michael.william@ti.ukdw.ac.id      |9781338132311  |5              |nice book        |2021-05-05 10:00:00 |Save Berhasil  |
    |ananda.kusumawardana@ti.ukdw.ac.id |9781338132311  |5              |nice book        |2021-06-11 11:00:00 |Save Berhasil  |

  Scenario Outline: Menambahkan data Review dan Gagal
    Given Terdapat data-data Review
      |idReview |isbn               |emailUser                            |rating |review               |timeReview           |
      |1        |9781338132311      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Buku yang Bagus      |2021-06-11 13:00:00  |
      |2        |9780804171588      |nathaniel.alvin@ti.ukdw.ac.id        |5      |Romancenya bagus     |2021-04-10 10:00:00  |
      |3        |9780804171588      |michael.william@ti.ukdw.ac.id        |3.5    |NotBad               |2021-01-01 14:00:00  |
      |4        |9780804171588      |ananda.kusumawardana@ti.ukdw.ac.id   |4      |So so lah            |2021-02-03 15:00:00  |
    When user beremail <email> ingin menambahkan data review <isbn> , <rating> , <review> , <timeReview>
    Then user mendapatkan Bad Request dari data input <email> , <isbn> , <rating> , <review> , <timeReview>
    Examples:
      |email                              |isbn           |rating         |review           |timeReview         |
      |nathaniel.alvin@ti.ukdw.ac.id      |               |5              |nice book        |2021-05-10 11:00:00|
      |                                   |9781338132311  |0              |nice book        |2021-03-01 14:00:00|
      |                                   |               |               |                 |                   |