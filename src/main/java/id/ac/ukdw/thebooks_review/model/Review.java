package id.ac.ukdw.thebooks_review.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@Table(name = "review")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_review")
    private int idReview;

    @NotNull @NotEmpty
    @Column(name = "isbn")
    private String isbn;

    @NotNull @NotEmpty
    @Column(name = "email_user")
    private String emailUser;

    @Size(min = 0, max = 5)
    @Column(name = "rating")
    private double rating;

    @Column(name = "review",columnDefinition = "longtext")
    private String review;

    @NotNull @NotEmpty
    @Column(name = "time_review",columnDefinition = "datetime")
    private String timeReview;
    
    public Review(){}
    
    public Review(String isbn, String emailUser, double rating, String review, String timeReview){
        this.isbn = isbn;
        this.emailUser = emailUser;
        this.rating = rating;
        this.review = review;
        this.timeReview = timeReview;
    }
}