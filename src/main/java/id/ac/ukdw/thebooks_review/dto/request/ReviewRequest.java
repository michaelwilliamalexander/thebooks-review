package id.ac.ukdw.thebooks_review.dto.request;

import lombok.Data;

@Data
public class ReviewRequest {
    
    private String isbn;
    
    private String emailUser;
    
    private double rating;
    
    private String review;

    private String timeReview;

    public ReviewRequest(){}

    public ReviewRequest(String isbn,
                         String emailUser,
                         double rating,
                         String review,
                         String timeReview){
        this.isbn = isbn;
        this.emailUser = emailUser;
        this.rating = rating;
        this.review = review;
        this.timeReview = timeReview;
    }
}
