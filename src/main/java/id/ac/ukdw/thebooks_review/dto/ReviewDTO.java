package id.ac.ukdw.thebooks_review.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ReviewDTO {

    private int idReview;

    private String isbn;

    private String emailUser;

    private double rating;

    private String review;

    private String timeReview;
}
