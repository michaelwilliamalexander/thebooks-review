package id.ac.ukdw.thebooks_review.service;

import id.ac.ukdw.thebooks_review.dto.ReviewDTO;
import id.ac.ukdw.thebooks_review.exception.BadRequestException;
import id.ac.ukdw.thebooks_review.exception.NotFoundException;
import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.repository.dao.ReviewDao;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.valid4j.Validation.validate;

@Service
@RequiredArgsConstructor
public class ReviewService {

    private final ReviewDao repository;

    private final ModelMapper mapper;

    public List<ReviewDTO> getAllReview(String isbn){
        Optional<List<Review>>reviewList = repository.findByIsbn(isbn);
        return getDataReview(reviewList);
    }

    public ReviewDTO getDetailReview(int id){
        Optional<Review> find = repository.findById(id);
        if(find.isPresent()){
            ReviewDTO data = mapper.map(find.get(),ReviewDTO.class);
            return data;
        }
        throw new NotFoundException();
    }

    public List<ReviewDTO> getAllReviewByEmail(String email){
        Optional<List<Review>> reviewList = repository.findByEmail(email);
        return getDataReview(reviewList);
    }

    private List<ReviewDTO> getDataReview(Optional<List<Review>> reviewList) {
        if (reviewList.isPresent()){
            List<ReviewDTO> result = new ArrayList<>();
            for (Review data:reviewList.get()){
                result.add(mapper.map(data,ReviewDTO.class));
            }
            return result;
        }
        throw new NotFoundException();
    }

    public String saveReview(String isbn,
                             String emailUser,
                             double rating,
                             String review,
                             String timeReview){

        validate(!isbn.isEmpty(), new BadRequestException());
        validate(!emailUser.isEmpty(),new BadRequestException());
        validate(isbn !=null, new BadRequestException());
        validate(emailUser !=null, new BadRequestException());

        Optional<Review> find = repository.findByIsbnAndEmail(isbn,emailUser);
        if(find.isEmpty()){
            Review data = new Review(isbn,emailUser,rating,review,timeReview);
            if(repository.save(data)) {
                return "Save berhasil";
            }
        }
        throw new BadRequestException();
    }

}
