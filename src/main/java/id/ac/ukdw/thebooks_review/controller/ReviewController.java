package id.ac.ukdw.thebooks_review.controller;

import id.ac.ukdw.thebooks_review.dto.request.*;
import id.ac.ukdw.thebooks_review.dto.response.ResponseWrapper;
import id.ac.ukdw.thebooks_review.exception.BadRequestException;
import id.ac.ukdw.thebooks_review.service.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import static id.ac.ukdw.thebooks_review.config.SwaggerConfig.reviewServiceTag;
import static org.valid4j.Validation.validate;

@Controller
@Api(tags = reviewServiceTag)
@RequestMapping("/review")
@RequiredArgsConstructor
public class ReviewController {

    private final ReviewService service;

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Melihat detail detail review")
    public ResponseEntity<ResponseWrapper> getDetailReview(@PathVariable("id") Integer id){
        return ResponseEntity.ok(new ResponseWrapper(
                service.getDetailReview(id)));
    }

    @GetMapping(value ="/buku/{isbn}", produces= MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan Semua Data Review ")
    public ResponseEntity<ResponseWrapper> getAllReviewofABook(@PathVariable("isbn") String isbn){
        return ResponseEntity.ok(
            new ResponseWrapper(
                service.getAllReview(isbn)
            )
        );
    }


    @GetMapping(value = "/user/",produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Mendapatkan semua data review user")
    public ResponseEntity<ResponseWrapper> getAllReviewUser(@RequestParam("email") String email){
        return ResponseEntity.ok(new ResponseWrapper(service.getAllReviewByEmail(email)));
    }

    @PostMapping(value="/", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Menyimpan data Review")
    public ResponseEntity<ResponseWrapper> saveReview(
            @ApiParam(value = "format Tanggal YYYY-MM-DD HH:MM:SS")
            @RequestBody ReviewRequest request){
        return ResponseEntity.ok(new ResponseWrapper(
                service.saveReview(
                        request.getIsbn(),
                        request.getEmailUser(),
                        request.getRating(),
                        request.getReview(),
                        request.getTimeReview()
                )
            )
        );
    }

}
