package id.ac.ukdw.thebooks_review.repository;

import id.ac.ukdw.thebooks_review.model.Review;
import id.ac.ukdw.thebooks_review.repository.dao.ReviewDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
@RequiredArgsConstructor
public class ReviewRepository implements ReviewDao {

    private final EntityManager entityManager;

    @Override
    public Optional<List<Review>> findByIsbn(String isbn) {
        String hql = "select rvw from Review rvw where rvw.isbn = :isbn";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn", isbn);
        List<Review> result = query.getResultList();
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<List<Review>> findByEmail(String email) {
        String hql = "select rvw from Review rvw where rvw.emailUser = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("email", email);
        List<Review> result = query.getResultList();
        return Optional.ofNullable(result);
    }

    @Override
    public Optional<Review> findByIsbnAndEmail(String isbn, String email) {
        String hql = "select rvw from Review rvw where rvw.isbn = :isbn and rvw.emailUser = :email";
        Query query = entityManager.createQuery(hql);
        query.setParameter("isbn",isbn);
        query.setParameter("email",email);
        List<Review> result = query.getResultList();
        Review review = null;
        for (Review data: result){
            review = data;
        }
        return Optional.ofNullable(review);
    }

    @Override
    public Optional<Review> findById(Integer id) {
        String hql = "select rvw from Review rvw where rvw.idReview = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id",id);
        List<Review> result = query.getResultList();
        Review review = null;
        for (Review data: result){
            review = data;
        }
        return Optional.ofNullable(review);
    }

    @Override
    public boolean save(Review review) {
        String hql = "insert into review(isbn,email_user,rating,review,time_review) values(?,?,?,?,?)";
        Query query = entityManager.createNativeQuery(hql);
        query.setParameter(1,review.getIsbn());
        query.setParameter(2,review.getEmailUser());
        query.setParameter(3,review.getRating());
        query.setParameter(4,review.getReview());
        query.setParameter(5,review.getTimeReview());
        query.executeUpdate();
        return true;
    }

}
