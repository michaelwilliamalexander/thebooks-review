package id.ac.ukdw.thebooks_review.repository.dao;

import java.util.List;
import java.util.Optional;

public interface Dao <T,ID>{

    Optional<T> findById(ID id);

    boolean save(T t);

}
