package id.ac.ukdw.thebooks_review.repository.dao;

import id.ac.ukdw.thebooks_review.model.Review;

import java.util.List;
import java.util.Optional;

public interface ReviewDao extends Dao<Review, Integer>{
    
    Optional<List<Review>> findByIsbn(String isbn);

    Optional<List<Review>> findByEmail(String email);
    
    Optional<Review> findByIsbnAndEmail(String isbn, String email);
}
