package id.ac.ukdw.thebooks_review.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String reviewServiceTag = "Review Service";

    @Bean
    public Docket authenticationApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("id.ac.ukdw.thebooks_review.controller"))
                .build()
                .apiInfo(metadata())
                .tags(new Tag(reviewServiceTag,"REST API untuk review buku aplikasi The Books"));
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("The Books")
                .description("Dokumentasi API Review Service")
                .build();
    }
}
